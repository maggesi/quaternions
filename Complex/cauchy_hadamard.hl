(* ========================================================================= *)
(* Cauchy-Hadamard formula for radius of convergence of real and complex     *)
(* power series and their derivative.                                        *)
(*                                                                           *)
(* (c) Copyright, Andrea Gabrielli, Marco Maggesi 2016-2017                  *)
(* ========================================================================= *)

(* ------------------------------------------------------------------------- *)
(* It is always possible to shrink a ball containing a compact set.          *)
(* ------------------------------------------------------------------------- *)

let COMPACT_SHRINK_ENCLOSING_BALL = prove
 (`!s x:real^N r. &0 < r /\ compact s /\ s SUBSET ball(x,r)
                  ==> ?r'. &0 < r' /\ r' < r /\ s SUBSET ball(x,r')`,
  REPEAT GEN_TAC THEN ASM_CASES_TAC `s:real^N->bool = {}` THENL
  [POP_ASSUM SUBST1_TAC THEN INTRO_TAC "r _ _" THEN EXISTS_TAC `r / &2` THEN
   REWRITE_TAC[EMPTY_SUBSET] THEN ASM_REAL_ARITH_TAC;
   ALL_TAC] THEN
  REWRITE_TAC[SUBSET; IN_BALL] THEN REPEAT STRIP_TAC THEN
  MP_TAC (ISPECL[`s:real^N->bool`;`x:real^N`] DISTANCE_ATTAINS_SUP) THEN
  ASM_REWRITE_TAC[] THEN INTRO_TAC "@y. y le" THEN
  EXISTS_TAC `(dist (x:real^N,y) + r) / &2` THEN
  CLAIM_TAC "rlt" `dist(x:real^N,y) < r` THENL [ASM_SIMP_TAC[]; ALL_TAC] THEN
  ASM_SIMP_TAC[REAL_ARITH `a <= b /\ b < c ==> a < (b + c) / &2`] THEN
  ASM_NORM_ARITH_TAC);;

(* ------------------------------------------------------------------------- *)
(* A version of the previous theorem formulated in a way that it can         *)
(* specialize to the case of the ball with infinite radious.  This turns to  *)
(* be useful to work with the radious of convercence of a power series.      *)
(* ------------------------------------------------------------------------- *)

let COMPACT_SHRINK_ENCLOSING_BALL_INFTY = prove
 (`!s b. compact s /\ (!x:real^N. x IN s ==> b * norm x < &1)
         ==> ?r. &0 < r /\ b * r < &1 /\ !x. x IN s ==> norm x < r`,
  INTRO_TAC "!s b; cpt sub" THEN ASM_CASES_TAC `&0 < b` THENL
  [MP_TAC (ISPECL [`s:real^N->bool`;`vec 0:real^N`; `inv b`]
                  COMPACT_SHRINK_ENCLOSING_BALL) THEN
   ASM_REWRITE_TAC[REAL_LT_INV_EQ; SUBSET; IN_BALL_0] THEN
   ANTS_TAC THENL
   [GEN_TAC THEN DISCH_TAC THEN
    MATCH_MP_TAC (SPEC `b:real` REAL_LT_LCANCEL_IMP) THEN
    ASM_SIMP_TAC[REAL_POS_NZ; REAL_MUL_RINV];
    ALL_TAC] THEN
   INTRO_TAC "@r. rpos rlt sub" THEN EXISTS_TAC `r:real` THEN
   ASM_REWRITE_TAC[] THEN MATCH_MP_TAC (SPEC `inv b` REAL_LT_LCANCEL_IMP) THEN
   ASM_REWRITE_TAC[REAL_LT_INV_EQ; REAL_MUL_RID] THEN
   ASM_SIMP_TAC[REAL_FIELD `&0 < b ==> inv b * b * r = r`];
   POP_ASSUM (LABEL_TAC "bneg" o REWRITE_RULE[REAL_NOT_LT])] THEN
  ASM_CASES_TAC `s:real^N->bool = {}` THENL
  [POP_ASSUM SUBST1_TAC THEN EXISTS_TAC `&1` THEN
   REWRITE_TAC[REAL_LT_01; NOT_IN_EMPTY] THEN ASM_REAL_ARITH_TAC;
   ALL_TAC] THEN
  MP_TAC (ISPECL [`s:real^N->bool`; `vec 0:real^N`] DISTANCE_ATTAINS_SUP) THEN
  ASM_REWRITE_TAC[DIST_0] THEN INTRO_TAC "@x. x le" THEN
  EXISTS_TAC `norm (x:real^N) + &1` THEN
  CONJ_TAC THENL [ASM_NORM_ARITH_TAC; ALL_TAC] THEN
  ASM_SIMP_TAC[REAL_ARITH `x <= y ==> x < y + &1`] THEN
  TRANS_TAC REAL_LET_TRANS `&0` THEN
  REWRITE_TAC[REAL_LT_01; GSYM REAL_NOT_LT; REAL_MUL_POS_LT] THEN
  ASM_NORM_ARITH_TAC);;

(* ------------------------------------------------------------------------- *)
(* Convergence and absolute convergence for complex power series.            *)
(* ------------------------------------------------------------------------- *)

let CAUCHY_HADAMARD_RADIUS_ABSCONV = prove
 (`!a k b z:complex.
     ((\n. root n (norm (a n))) has_limsup b) (sequentially within k) /\
     b * norm z < &1
     ==> real_summable k (\n. norm (a n * z pow n))`,
  INTRO_TAC "!a k b z; lim radius" THEN
  ASM_CASES_TAC `FINITE (k:num->bool)` THENL
  [ASM_SIMP_TAC[REAL_SUMMABLE_FINITE]; POP_ASSUM (LABEL_TAC "fin")] THEN
  MATCH_MP_TAC REAL_SERIES_ROOT_TEST THEN
  EXISTS_TAC `b:real * norm (z:complex)` THEN ASM_REWRITE_TAC[NORM_POS_LE] THEN
  REWRITE_TAC[COMPLEX_NORM_MUL; REAL_ROOT_MUL; COMPLEX_NORM_POW] THEN
  MATCH_MP_TAC HAS_LIMSUP_MUL_REALLIM_RIGHT THEN ASM_REWRITE_TAC[] THEN
  ASM_SIMP_TAC[ROOT_LE_0; REAL_POW_LE; NORM_POS_LE; EVENTUALLY_TRUE] THEN
  MATCH_MP_TAC REALLIM_EVENTUALLY THEN
  ASM_REWRITE_TAC[EVENTUALLY_SEQUENTIALLY_WITHIN] THEN EXISTS_TAC `1` THEN
  REPEAT STRIP_TAC THEN MATCH_MP_TAC REAL_ROOT_POW THEN
  REWRITE_TAC[NORM_POS_LE] THEN ASM_ARITH_TAC);;

let CAUCHY_HADAMARD_RADIUS = prove
 (`!a k b z:complex.
     ((\n. root n (norm (a n))) has_limsup b) (sequentially within k) /\
     b * norm z < &1
     ==> summable k (\n. a n * z pow n)`,
  REPEAT STRIP_TAC THEN MATCH_MP_TAC SERIES_NORMCONV_IMP_CONV THEN
  BETA_TAC THEN MATCH_MP_TAC CAUCHY_HADAMARD_RADIUS_ABSCONV THEN
  EXISTS_TAC `b:real` THEN ASM_REWRITE_TAC[]);;

let CAUCHY_HADAMARD_RADIUS_ABSCONV_DERIVATIVE = prove
 (`!z a k b.
     ((\n. root n (norm (a n))) has_limsup b) (sequentially within k) /\
     b * norm z < &1
     ==> real_summable k (\n. norm (Cx(&n) * a n * z pow (n - 1)))`,
  REPEAT GEN_TAC THEN INTRO_TAC "limsup norm" THEN
  ASM_CASES_TAC `FINITE (k:num->bool)` THENL
  [ASM_SIMP_TAC[REAL_SUMMABLE_FINITE]; POP_ASSUM (LABEL_TAC "fin")] THEN
  MATCH_MP_TAC REAL_SERIES_ROOT_TEST THEN EXISTS_TAC `b * norm(z:complex)` THEN
  ASM_REWRITE_TAC[NORM_POS_LE] THEN ASM_CASES_TAC `norm(z:complex) = &0` THENL
  [ASM_REWRITE_TAC[COMPLEX_NORM_MUL; COMPLEX_NORM_CX; REAL_ROOT_MUL;
                   COMPLEX_NORM_POW] THEN
   MATCH_MP_TAC HAS_LIMSUP_TRANSFORM THEN EXISTS_TAC `\n:num. &0` THEN
   REWRITE_TAC[REAL_MUL_RZERO] THEN
   SIMP_TAC[REALLIM_IMP_HAS_LIMSUP; REALLIM_CONST] THEN
   ASM_REWRITE_TAC[EVENTUALLY_SEQUENTIALLY_WITHIN] THEN
   EXISTS_TAC `2` THEN INTRO_TAC "!n; n le" THEN
   REWRITE_TAC[REAL_POW_ZERO] THEN
   ASM_SIMP_TAC[ARITH_RULE `2 <= n ==> ~(n - 1 = 0)`] THEN
   REWRITE_TAC[ROOT_0; REAL_MUL_RZERO];
   ALL_TAC] THEN
  CLAIM_TAC "pos" `&0 < norm(z:complex)` THENL
  [ASM_MESON_TAC[NORM_POS_LE;
                 REAL_ARITH`!x:real. &0 <= x <=> x = &0 \/ &0 < x`];
   ALL_TAC] THEN
  REWRITE_TAC[COMPLEX_NORM_MUL; COMPLEX_NORM_CX; COMPLEX_NORM_POW;
              REAL_ROOT_MUL; REAL_ABS_NUM; GSYM REAL_MUL_ASSOC] THEN
  GEN_REWRITE_TAC (RATOR_CONV o RAND_CONV) [GSYM REAL_MUL_LID] THEN
  MATCH_MP_TAC HAS_LIMSUP_MUL_REALLIM_LEFT THEN
  SIMP_TAC[REALLIM_ROOT_REFL; REALLIM_SEQUENTIALLY_WITHIN] THEN
  REWRITE_TAC[ROOT_LE_0; REAL_POS; EVENTUALLY_TRUE] THEN CONJ_TAC THENL
  [ALL_TAC;
   MATCH_MP_TAC ALWAYS_EVENTUALLY THEN GEN_TAC THEN
   BETA_TAC THEN MATCH_MP_TAC REAL_LE_MUL THEN
   SIMP_TAC[ROOT_LE_0; REAL_POW_LE; NORM_POS_LE]] THEN
  MATCH_MP_TAC HAS_LIMSUP_MUL_REALLIM_RIGHT THEN ASM_REWRITE_TAC[] THEN
  SIMP_TAC[ROOT_LE_0; NORM_POS_LE; REAL_POW_LE; EVENTUALLY_TRUE] THEN
  MATCH_MP_TAC REALLIM_TRANSFORM_EVENTUALLY THEN
  EXISTS_TAC `\n:num. norm(z:complex) rpow (&1 - inv(&n))` THEN CONJ_TAC THENL
  [ASM_REWRITE_TAC[EVENTUALLY_SEQUENTIALLY_WITHIN] THEN
   EXISTS_TAC `2` THEN INTRO_TAC "!n; n le" THEN
   ASM_SIMP_TAC[REAL_ROOT_RPOW; REAL_POW_LE; NORM_POS_LE; GSYM RPOW_POW;
                RPOW_RPOW; ARITH_RULE `2 <= n ==> ~(n = 0)`] THEN
   AP_TERM_TAC THEN
   ASM_SIMP_TAC[GSYM REAL_OF_NUM_SUB; ARITH_RULE `2 <= n ==> 1 <= n`] THEN
   CUT_TAC `&0 < &n` THENL [CONV_TAC REAL_FIELD; ALL_TAC] THEN
   REWRITE_TAC[REAL_OF_NUM_LT] THEN ASM_ARITH_TAC;
   ALL_TAC] THEN
  SUBGOAL_THEN `!f:num->real a. (f ---> a) = (f ---> a rpow &1)`
    (fun th -> ONCE_REWRITE_TAC[th]) THENL
  [REWRITE_TAC[RPOW_POW; REAL_POW_1]; ALL_TAC] THEN
  MATCH_MP_TAC REALLIM_RPOW_COMPOSE THEN ASM_REWRITE_TAC[] THEN CONJ_TAC THENL
  [ASM_SIMP_TAC[REALLIM_CONST; REALLIM_SEQUENTIALLY_WITHIN]; ALL_TAC] THEN
  GEN_REWRITE_TAC (RATOR_CONV o RAND_CONV) [REAL_ARITH `&1 = &1 - &0`] THEN
  MATCH_MP_TAC REALLIM_SUB THEN
  SIMP_TAC[REALLIM_CONST; REALLIM_1_OVER_N; REALLIM_SEQUENTIALLY_WITHIN]);;

let CAUCHY_HADAMARD_RADIUS_DERIVATIVE = prove
 (`!z a k b.
      ((\n. root n (norm (a n))) has_limsup b) (sequentially within k) /\
      b * norm z < &1
      ==> summable k (\n. Cx(&n) * a n * z pow (n - 1))`,
  REPEAT GEN_TAC THEN INTRO_TAC "limsup norm" THEN
  MATCH_MP_TAC SERIES_NORMCONV_IMP_CONV THEN
  ASM_MESON_TAC[CAUCHY_HADAMARD_RADIUS_ABSCONV_DERIVATIVE]);;

(* ------------------------------------------------------------------------- *)
(* Convergence and absolute convergence for real power series.               *)
(* ------------------------------------------------------------------------- *)

let REAL_CAUCHY_HADAMARD_RADIUS_ABSCONV = prove
 (`!a k b x.
     ((\n. root n (abs (a n))) has_limsup b) (sequentially within k) /\
     b * abs x < &1
     ==> real_summable k (\n. abs (a n * x pow n))`,
  REPEAT STRIP_TAC THEN
  MP_TAC (SPECL [`\n:num. Cx(a n)`; `k:num->bool`; `b:real`; `Cx x`]
                CAUCHY_HADAMARD_RADIUS_ABSCONV) THEN
  ASM_REWRITE_TAC[GSYM CX_POW; GSYM CX_MUL; COMPLEX_NORM_CX]);;

let REAL_CAUCHY_HADAMARD_RADIUS = prove
 (`!a k b x.
     ((\n. root n (abs (a n))) has_limsup b) (sequentially within k) /\
     b * abs x < &1
     ==> real_summable k (\n. a n * x pow n)`,
  REPEAT STRIP_TAC THEN MATCH_MP_TAC REAL_SERIES_ABSCONV_IMP_CONV THEN
  BETA_TAC THEN MATCH_MP_TAC REAL_CAUCHY_HADAMARD_RADIUS_ABSCONV THEN
  EXISTS_TAC `b:real` THEN ASM_REWRITE_TAC[]);;

let REAL_CAUCHY_HADAMARD_RADIUS_ABSCONV_DERIVATIVE = prove
 (`!x a k b.
     ((\n. root n (abs (a n))) has_limsup b) (sequentially within k) /\
     b * abs x < &1
     ==> real_summable k (\n. abs (&n * a n * x pow (n - 1)))`,
  REPEAT STRIP_TAC THEN
  MP_TAC (SPECL [`Cx x`; `\n:num. Cx(a n)`; `k:num->bool`; `b:real`]
                CAUCHY_HADAMARD_RADIUS_ABSCONV_DERIVATIVE) THEN
  ASM_REWRITE_TAC[COMPLEX_NORM_CX; GSYM CX_POW; GSYM CX_MUL]);;

let REAL_CAUCHY_HADAMARD_RADIUS_DERIVATIVE = prove
 (`!x a k b.
      ((\n. root n (abs (a n))) has_limsup b) (sequentially within k) /\
      b * abs x < &1
      ==> real_summable k (\n. &n * a n * x pow (n - 1))`,
  REPEAT GEN_TAC THEN INTRO_TAC "limsup norm" THEN
  MATCH_MP_TAC REAL_SERIES_ABSCONV_IMP_CONV THEN
  ASM_MESON_TAC[REAL_CAUCHY_HADAMARD_RADIUS_ABSCONV_DERIVATIVE]);;

(* ------------------------------------------------------------------------- *)
(* Uniform convergence for complex power series.                             *)
(* ------------------------------------------------------------------------- *)

let CAUCHY_HADAMARD_RADIUS_UNIFORM = prove
 (`!a b s k.
     ((\n. root n (norm (a n))) has_limsup b) (sequentially within k) /\
     compact s /\
     (!z:complex. z IN s ==> b * norm z < &1)
     ==> ?l. !e. &0 < e
                 ==> ?N. !n z. N <= n /\ z IN s
                         ==> dist(vsum (k INTER (0..n)) (\i. a i * z pow i),
                                  l z) < e`,
  INTRO_TAC "!a b s k; limsup cpt sub" THEN
  ASM_CASES_TAC `FINITE (k:num->bool)` THENL
  [POP_ASSUM MP_TAC THEN REWRITE_TAC[num_FINITE] THEN INTRO_TAC "@N. N" THEN
   EXISTS_TAC `\z:complex. vsum k (\i. a i * z pow i)` THEN
   INTRO_TAC "!e; epos" THEN EXISTS_TAC `N:num` THEN INTRO_TAC "!n z; n z" THEN
   SUBGOAL_THEN `k INTER (0..n) = k`
     (fun th -> ASM_REWRITE_TAC[th; DIST_REFL]) THEN
   REWRITE_TAC[EXTENSION; IN_INTER; IN_NUMSEG; LE_0] THEN
   GEN_TAC THEN ASM_CASES_TAC `x:num IN k` THEN ASM_REWRITE_TAC[] THEN
   TRANS_TAC LE_TRANS `N:num` THEN ASM_SIMP_TAC[];
   POP_ASSUM (LABEL_TAC "fin")] THEN
  MP_TAC (ISPECL [`s:complex->bool`;`b:real`]
                 COMPACT_SHRINK_ENCLOSING_BALL_INFTY) THEN
  ANTS_TAC THENL [ASM_REWRITE_TAC[]; ALL_TAC] THEN
  INTRO_TAC "@r. r0 r1 r2" THEN
  MP_TAC (ISPECL[`\z:complex n. a n * z pow n`;
                 `\n. norm(a n:complex) * r pow n`;
                 `\z:complex. z IN s`; `k:num->bool`]
                SERIES_COMPARISON_UNIFORM) THEN
  ANTS_TAC THENL [ALL_TAC; MESON_TAC[]] THEN CONJ_TAC THENL
  [REWRITE_TAC[GSYM summable; GSYM REAL_SUMMABLE] THEN
   MATCH_MP_TAC REAL_CAUCHY_HADAMARD_RADIUS THEN
   EXISTS_TAC `b:real` THEN ASM_REWRITE_TAC[REAL_ABS_NORM] THEN
   ASM_SIMP_TAC[real_abs; REAL_LT_IMP_LE];
   ALL_TAC] THEN
  EXISTS_TAC `0` THEN REWRITE_TAC[LE_0] THEN INTRO_TAC "!n [z]; n z" THEN
  REWRITE_TAC[COMPLEX_NORM_MUL; COMPLEX_NORM_POW] THEN
  MATCH_MP_TAC REAL_LE_LMUL THEN
  ASM_SIMP_TAC[REAL_POW_LE2; NORM_POS_LE; REAL_LT_IMP_LE]);;

let CAUCHY_HADAMARD_RADIUS_UNIFORM_DERIVATIVE = prove
 (`!a b s k.
     ((\n. root n (norm (a n))) has_limsup b) (sequentially within k) /\
     compact s /\
     (!z. z IN s ==> b * norm z < &1)
     ==> ?l. !e. &0 < e
                 ==> ?N. !n z.
                       N <= n /\ z IN s
                       ==> dist(vsum (k INTER (0..n))
                                     (\i. Cx(&i) * a i * z pow (i - 1)),
                                l z) < e`,
  INTRO_TAC "!a s b k; limsup compact sub" THEN
  ASM_CASES_TAC `FINITE (k:num->bool)` THENL
  [POP_ASSUM MP_TAC THEN REWRITE_TAC[num_FINITE] THEN INTRO_TAC "@N. N" THEN
   EXISTS_TAC `\z. vsum k (\i. Cx (&i) * a i * z pow (i - 1))` THEN
   INTRO_TAC "!e; epos" THEN EXISTS_TAC `N:num` THEN INTRO_TAC "!n z; n z" THEN
   SUBGOAL_THEN `k INTER (0..n) = k`
     (fun th -> ASM_REWRITE_TAC[th; DIST_REFL]) THEN
   REWRITE_TAC[EXTENSION; IN_INTER; IN_NUMSEG; LE_0] THEN GEN_TAC THEN
   ASM_CASES_TAC `x:num IN k` THEN ASM_REWRITE_TAC[] THEN
   TRANS_TAC LE_TRANS `N:num` THEN ASM_SIMP_TAC[];
   POP_ASSUM (LABEL_TAC "fin")] THEN
  CLAIM_TAC "@r. r1 r2 r3"
    `?r. &0 < r /\ b * r < &1 /\ (!z:complex. z IN s ==> norm z < r)` THENL
  [MATCH_MP_TAC COMPACT_SHRINK_ENCLOSING_BALL_INFTY THEN ASM_REWRITE_TAC[];
   ALL_TAC] THEN
  MP_TAC (ISPECL[`\z:complex n. Cx(&n) * a n * z pow (n - 1)`;
                 `\n. &n * norm(a n:complex) * r pow (n - 1)`;
                 `\z:complex. z IN s`; `k:num->bool`]
                SERIES_COMPARISON_UNIFORM) THEN
  ANTS_TAC THENL [ALL_TAC; MESON_TAC[]] THEN CONJ_TAC THENL
  [REWRITE_TAC[GSYM summable; GSYM REAL_SUMMABLE] THEN
   MATCH_MP_TAC REAL_CAUCHY_HADAMARD_RADIUS_DERIVATIVE THEN
   EXISTS_TAC `b:real` THEN ASM_REWRITE_TAC[REAL_ABS_NORM] THEN
   ASM_SIMP_TAC[real_abs; REAL_LT_IMP_LE];
   ALL_TAC] THEN
  EXISTS_TAC `0` THEN REWRITE_TAC[LE_0] THEN INTRO_TAC "!n [z]; n z" THEN
  REWRITE_TAC[COMPLEX_NORM_MUL; COMPLEX_NORM_POW; COMPLEX_NORM_CX;
              REAL_ABS_NUM; REAL_MUL_ASSOC] THEN
  MATCH_MP_TAC REAL_LE_LMUL THEN
  ASM_SIMP_TAC[REAL_POW_LE2; NORM_POS_LE; REAL_LT_IMP_LE;
               REAL_LE_MUL; REAL_OF_NUM_LE; LE_0]);;

(* ------------------------------------------------------------------------- *)
(* Uniform convergence for real power series.                                *)
(* ------------------------------------------------------------------------- *)

let REAL_CAUCHY_HADAMARD_RADIUS_UNIFORM = prove
 (`!a b s k.
     ((\n. root n (abs (a n))) has_limsup b) (sequentially within k) /\
     real_compact s /\
     (!x:real. x IN s ==> b * abs x < &1)
     ==> ?l. !e. &0 < e
                 ==> ?N. !n x. N <= n /\ x IN s
                         ==> abs(sum (k INTER (0..n)) (\i. a i * x pow i) -
                                  l x) < e`,
  INTRO_TAC "!a b s k; limsup cpt sub" THEN
  ASM_CASES_TAC `FINITE (k:num->bool)` THENL
  [POP_ASSUM MP_TAC THEN REWRITE_TAC[num_FINITE] THEN INTRO_TAC "@N. N" THEN
   EXISTS_TAC `\x:real. sum k (\i. a i * x pow i)` THEN
   INTRO_TAC "!e; epos" THEN EXISTS_TAC `N:num` THEN INTRO_TAC "!n x; n x" THEN
   SUBGOAL_THEN `k INTER (0..n) = k`
     (fun th -> ASM_REWRITE_TAC[th; REAL_SUB_REFL;REAL_ABS_0]) THEN
   REWRITE_TAC[EXTENSION; IN_INTER; IN_NUMSEG; LE_0] THEN
   GEN_TAC THEN ASM_CASES_TAC `x':num IN k` THEN ASM_REWRITE_TAC[] THEN
   TRANS_TAC LE_TRANS `N:num` THEN ASM_SIMP_TAC[];
   POP_ASSUM (LABEL_TAC "fin")] THEN
  HYP_TAC "cpt" (REWRITE_RULE[real_compact]) THEN
  MP_TAC (ISPECL [`IMAGE lift (s:real->bool)`;`b:real`]
                 COMPACT_SHRINK_ENCLOSING_BALL_INFTY) THEN
  ANTS_TAC THENL
 [ASM_REWRITE_TAC[IN_IMAGE] THEN GEN_TAC THEN INTRO_TAC "@x. lift" THEN
  ASM_SIMP_TAC[NORM_LIFT];
  ALL_TAC] THEN
  REWRITE_TAC[IN_IMAGE] THEN INTRO_TAC "@r. (r0 r1 r2)" THEN
  MP_TAC (ISPECL [`\x:real n. lift (a n * x pow n)`;
                  `\n. abs(a n:real) * r pow n`;
                  `\x:real. x IN s`; `k:num->bool`]
                 SERIES_COMPARISON_UNIFORM) THEN
  ANTS_TAC THENL
  [ALL_TAC;
   REWRITE_TAC[SUM_VSUM; o_DEF; DIST_1] THEN INTRO_TAC "@l. l" THEN
   EXISTS_TAC `(\x:real. drop (l x))` THEN ASM_MESON_TAC[]] THEN
  CONJ_TAC THENL
  [REWRITE_TAC[GSYM summable; GSYM REAL_SUMMABLE] THEN
   MATCH_MP_TAC REAL_CAUCHY_HADAMARD_RADIUS THEN EXISTS_TAC `b:real` THEN
   ASM_REWRITE_TAC[REAL_ABS_ABS] THEN ASM_SIMP_TAC[real_abs; REAL_LT_IMP_LE];
   ALL_TAC] THEN
  EXISTS_TAC `0` THEN REWRITE_TAC[LE_0] THEN INTRO_TAC "!n [x]; n x" THEN
  REWRITE_TAC[NORM_LIFT; REAL_ABS_MUL; REAL_ABS_POW] THEN
  MATCH_MP_TAC REAL_LE_LMUL THEN
  CLAIM_TAC "1" `norm (lift x) < r` THENL
  [REMOVE_THEN "r2" MATCH_MP_TAC THEN EXISTS_TAC `x:real` THEN
   ASM_REWRITE_TAC[];
   ALL_TAC] THEN
  HYP_TAC "1" (REWRITE_RULE[NORM_LIFT]) THEN
  ASM_SIMP_TAC[REAL_POW_LE2; REAL_ABS_POS; REAL_LT_IMP_LE]);;

let REAL_CAUCHY_HADAMARD_RADIUS_UNIFORM_DERIVATIVE = prove
 (`!a b s k.
     ((\n. root n (abs (a n))) has_limsup b) (sequentially within k) /\
     real_compact s /\
     (!x. x IN s ==> b * abs x < &1)
     ==> ?l. !e. &0 < e
                 ==> ?N. !n x.
                       N <= n /\ x IN s
                       ==> abs(sum (k INTER (0..n))
                                     (\i. &i * a i * x pow (i - 1)) -
                                l x) < e`,
  INTRO_TAC "!a s b k; limsup compact sub" THEN
  ASM_CASES_TAC `FINITE (k:num->bool)` THENL
  [POP_ASSUM MP_TAC THEN REWRITE_TAC[num_FINITE] THEN INTRO_TAC "@N. N" THEN
   EXISTS_TAC `\x. sum k (\i. &i * a i * x pow (i - 1))` THEN
   INTRO_TAC "!e; epos" THEN EXISTS_TAC `N:num` THEN INTRO_TAC "!n x; n x" THEN
   SUBGOAL_THEN `k INTER (0..n) = k`
     (fun th -> ASM_REWRITE_TAC[th; REAL_SUB_REFL; REAL_ABS_0]) THEN
   REWRITE_TAC[EXTENSION; IN_INTER; IN_NUMSEG; LE_0] THEN GEN_TAC THEN
   ASM_CASES_TAC `x':num IN k` THEN ASM_REWRITE_TAC[] THEN
   TRANS_TAC LE_TRANS `N:num` THEN ASM_SIMP_TAC[];
   POP_ASSUM (LABEL_TAC "fin")] THEN
  HYP_TAC "compact" (REWRITE_RULE[real_compact]) THEN
  MP_TAC (ISPECL [`IMAGE lift (s:real->bool)`;`b:real`]
                 COMPACT_SHRINK_ENCLOSING_BALL_INFTY) THEN
  ANTS_TAC THENL
  [ASM_REWRITE_TAC[IN_IMAGE] THEN GEN_TAC THEN INTRO_TAC "@x. lift" THEN
   ASM_SIMP_TAC[NORM_LIFT];
   ALL_TAC] THEN
  REWRITE_TAC[IN_IMAGE] THEN INTRO_TAC "@r. (r0 r1 r2)" THEN
  MP_TAC (ISPECL[`\x:real n. lift (&n * a n * x pow (n - 1))`;
                 `\n. &n * abs(a n:real) * r pow (n - 1)`;
                 `\x:real. x IN s`; `k:num->bool`]
                SERIES_COMPARISON_UNIFORM) THEN
  ANTS_TAC THENL
  [ALL_TAC;
   REWRITE_TAC[SUM_VSUM; o_DEF; DIST_1] THEN
   INTRO_TAC "@l. l" THEN EXISTS_TAC `(\x:real. drop (l x))` THEN
   ASM_MESON_TAC[]] THEN
  CONJ_TAC THENL
  [REWRITE_TAC[GSYM summable; GSYM REAL_SUMMABLE] THEN
   MATCH_MP_TAC REAL_CAUCHY_HADAMARD_RADIUS_DERIVATIVE THEN
   EXISTS_TAC `b:real` THEN ASM_REWRITE_TAC[REAL_ABS_ABS] THEN
   ASM_SIMP_TAC[real_abs; REAL_LT_IMP_LE];
   ALL_TAC] THEN
  EXISTS_TAC `0` THEN REWRITE_TAC[LE_0] THEN INTRO_TAC "!n [x]; n x" THEN
  REWRITE_TAC[NORM_LIFT; REAL_ABS_POW; REAL_ABS_MUL;
              REAL_ABS_NUM; REAL_MUL_ASSOC] THEN
  MATCH_MP_TAC REAL_LE_LMUL THEN
  CLAIM_TAC "1" `norm (lift x) < r` THENL
  [REMOVE_THEN "r2" MATCH_MP_TAC THEN EXISTS_TAC `x:real` THEN
   ASM_REWRITE_TAC[];
   ALL_TAC] THEN
  HYP_TAC "1" (REWRITE_RULE[NORM_LIFT]) THEN
  ASM_SIMP_TAC[REAL_POW_LE2; REAL_ABS_POS; REAL_LT_IMP_LE; REAL_LE_MUL;
               REAL_OF_NUM_LE; LE_0]);;
